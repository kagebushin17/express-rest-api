const express = require('express');

const router = express.Router();
const mongoose = require('mongoose');

const Role = require('../models/role');


// Handle incoming GET requests to /books
router.get('/', (req, res, next) => {
    Role.find()
        .exec()
        .then(docs => {
            console.log(docs);
            res.status(200).json(docs);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/', (req, res, next) => {

    const role = new Role({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name
    });
    role
    .save()
    .then(result => {
        console.log(result);
        res.status(201).json({
            message: 'Handling POST requests to /roles',
            createdRole: result
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err})
    });
});

router.get('/:roleId', (req, res, next) => {
    const id = req.params.roleId;
    Role.findById(id)
    .exec()
    //en el then se devuelven los resultados por el asincrono de js.
    .then(doc => {
        console.log(doc);
        if (doc) {
            res.status(200).json(doc);
        } else {
            res.status(404).json({
                message: 'No valid entry found for that ID'
            });
        }
    })
    //si hubiera un error se maneja dentro del catch
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});    
    });
});

router.patch('/:roleId', (req, res, next) => {
    const id = req.params.roleId;
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    Role.update({ _id : id }, { $set: updateOps })
        .exec()
        .then(result => {
            console.log(updateOps);
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.delete('/:roleId', (req, res, next) => {
    const id = req.params.roleId;
    Role.remove({_id : id})
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        })
});

module.exports = router;
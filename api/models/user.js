const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    fullName: {type: String, required: true},
    username: {type: String, required: true},
    password: {type: String, required: true},
    role: {type: mongoose.Schema.Types.ObjectId, ref: 'Role', required: true}
});


module.exports = mongoose.model('User', userSchema);
const express = require('express');

const router = express.Router();
const mongoose = require('mongoose');

const User = require('../models/user');
const Role = require('../models/role');


// Handle incoming GET requests to /users
router.get("/", (req, res, next) => {
    User.find()
      .select("fullName username password role _id")
      .exec()
      .then(docs => {
        res.status(200).json({
          count: docs.length,
          users: docs.map(doc => {
            return {
              _id: doc._id,
              fullName: doc.fullName,
              username: doc.username,
              password: doc.password,
              role: doc.role,
              request: {
                type: "GET",
                url: "http://localhost:3000/users/" + doc._id
              }
            };
          })
        });
      })
      .catch(err => {
        res.status(500).json({
          error: err
        });
      });
  });
  
  router.post("/", (req, res, next) => {
    Role.findById(req.body.roleId)
      .then(role => {
        if (!role) {
          return res.status(404).json({
            message: "Role not found"
          });
        }
        const user = new User({
          _id: mongoose.Types.ObjectId(),
          fullName: req.body.fullName,
          username: req.body.username,
          password: req.body.password,
          role: req.body.roleId
        });
        return user.save();
      })
      .then(result => {
        console.log(result);
        res.status(201).json({
          message: "User stored",
          createdUser: {
            _id: result._id,
            fullName: result.fullName,
            username: result.username,
            password: result.password,
            role: result.role
          },
          request: {
            type: "GET",
            url: "http://localhost:3000/users/" + result._id
          }
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });

  router.patch('/:userId', (req, res, next) => {
    const id = req.params.userId;
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    User.update({ _id : id }, { $set: updateOps })
        .exec()
        .then(result => {
            console.log(updateOps);
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
    });
  
  router.get("/:userId", (req, res, next) => {
    User.findById(req.params.userId)
      .exec()
      .then(user  => {
        if (!user) {
          return res.status(404).json({
            message: "User not found"
          });
        }
        res.status(200).json({
          user: user,
          request: {
            type: "GET",
            url: "http://localhost:3000/users"
          }
        });
      })
      .catch(err => {
        res.status(500).json({
          error: err
        });
      });
  });
  
  router.delete("/:userId", (req, res, next) => {
    User.remove({ _id: req.params.userId })
      .exec()
      .then(result => {
        res.status(200).json({
          message: "User deleted",
          request: {
            type: "POST",
            url: "http://localhost:3000/users",
            body: { roleId: "ID" }
          }
        });
      })
      .catch(err => {
        res.status(500).json({
          error: err
        });
      });
  });

module.exports = router;
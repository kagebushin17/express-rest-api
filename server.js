//----------- ALL REQUIRES HERE--------------------//
const http = require('http');
const app = require('./app');
//-------------------------------------------------//
//----------------- SERVER RUN VARIABLES-----------//
const port = process.env.PORT || 3000;

const server = http.createServer(app);

server.listen(port);
//-------------------------------------------------//
const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser =require('body-parser');
const mongoose = require('mongoose');


const userRoutes = require('./api/routes/users');
const roleRoutes = require('./api/routes/roles');

mongoose.connect('mongodb://127.0.0.1:27017/workshop3')
.then(()=> { console.log(`Succesfully Connected to the Mongodb Database  at URL : mongodb://127.0.0.1:27017/workshop3`)})
.catch(()=> { console.log(`Error Connecting to the Mongodb Database at URL : mongodb://127.0.0.1:27017/workshop3`)})

//es un paquete para logs segun los request al server
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Origin', 'Origin, X-Requested-With',
    'Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return rest.status(200).json({});
    }
    next();
});


//middleware para handlear los request
// Routes which should handle requests
app.use('/users', userRoutes);
app.use('/roles', roleRoutes);

//este middleware se usa despues de que no encontro ninguna otra ruta, quiere decir que es un 404.
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

//este middleware va a recoger el error anterior generado o cualquier otro tipo de error no esperado devolviendo un 500
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;